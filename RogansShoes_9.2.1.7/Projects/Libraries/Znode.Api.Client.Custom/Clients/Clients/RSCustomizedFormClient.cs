﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Api.Client.Custom.Endpoints;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Sample.Api.Model.RSCustomizedFormModel;

namespace Znode.Api.Client.Custom.Clients.Clients
{
    public class RSCustomizedFormClient : BaseClient, IRSCustomizedFormClient
    {
        public virtual bool ScheduleATruck(ScheduleATruckModel model)
        {
            string endpoint = RSCustomizedFormEndpoints.ScheduleATruck();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.IsSuccess;
        }


        public virtual bool ShoeFinder(ShoesFinderModel model)
        {
            string endpoint = RSCustomizedFormEndpoints.ShoeFinder();

            ApiStatus status = new ApiStatus();

            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.IsSuccess;
        }
    }
}
