﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;


namespace Znode.WebStore.Custom.Controllers
{
    public class RSCategoryController : CategoryController
    {
        private readonly ICategoryAgent _categoryAgent;
        public RSCategoryController(ICategoryAgent categoryAgent, IWidgetDataAgent widgetDataAgent) : base(categoryAgent, widgetDataAgent)
        {
            _categoryAgent = categoryAgent;
        }
        [HttpPost]
        public override JsonResult SiteMapList(int? pageSize, int? pageLength)
        {
            CategoryHeaderListViewModel cv = _categoryAgent.GetCategories(pageSize, pageLength);
            var jsonResult= Json(new { Result = _categoryAgent.GetCategories(pageSize, pageLength) }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;

        }
    }
}
