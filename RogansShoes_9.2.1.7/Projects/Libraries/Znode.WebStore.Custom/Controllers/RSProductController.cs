﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.WebStore.Custom.Controllers
{
   public class RSProductController : ProductController
    {
        #region Public Constructor

        private readonly IProductAgent _productAgent;
        public RSProductController(IProductAgent productAgent, IUserAgent userAgent, ICartAgent cartAgent, IWidgetDataAgent widgetDataAgent, IAttributeAgent attributeAgent):
            base(productAgent,userAgent,cartAgent,widgetDataAgent,attributeAgent)
        {
            _productAgent = productAgent;
            //_accountAgent = userAgent;
            //_cartAgent = cartAgent;
            //_widgetDataAgent = widgetDataAgent;
            //_attributeAgent = attributeAgent;
        }

        [HttpPost]
        //Get configurable data.
        public override ActionResult GetConfigurableProduct(ParameterProductModel model)
        {
            try
            {
                ZnodeLogging.LogMessage("RSProductController-GETCONFIGURABLE PRODUCT started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                ProductViewModel product = _productAgent.GetConfigurableProduct(model);
                ZnodeLogging.LogMessage("RSProductController-GETCONFIGURABLE PRODUCT Finished." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                product.IsQuickView = model.IsQuickView;
                product.IsProductEdit = !Equals(ViewBag.IsProductEdit, null) ? ViewBag.IsProductEdit : false;
                product.ProductTemplateName = string.IsNullOrEmpty(product.ProductTemplateName) || Equals(product.ProductTemplateName, ZnodeConstant.ProductDefaultTemplate) ? "Details" : product.ProductTemplateName;
                
                if (model.IsQuickView)
                {
                    ZnodeLogging.LogMessage("_QuickViewProductView Called-", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    return ActionView("_QuickViewProductView", product);
                }
                ZnodeLogging.LogMessage("RSProductController-GETCONFIGURABLE PRODUCT Finished.product.ProductTemplateName=" + product.ProductTemplateName, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return ActionView(product.ProductTemplateName, product);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("RSProductController-GETCONFIGURABLE PRODUCT ERROR." + ex.Message + "--" + ex.StackTrace, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return ActionView("_Big_View", new ProductViewModel());
            }

        }

        //[System.Web.Mvc.HttpGet]
        //[ChildActionOnly]
        //[ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;publishState")]
        //public override ActionResult BriefContent(int id = 0, string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
        //{
        //    ViewBag.ProductId = id;
        //    ViewBag.Seo = seo;
        //    ViewBag.IsQuickView = isQuickView;
        //    ViewBag.IsLite = true;

        //    ProductViewModel product = _productAgent.GetProductBrief(id);
        //    if (IsNull(product))
        //        return Redirect("/404");

        //    product.ProductTemplateName = string.IsNullOrEmpty(product.ProductTemplateName) || Equals(product.ProductTemplateName, ZnodeConstant.ProductDefaultTemplate) ? "Details" : product.ProductTemplateName;
        //    string templateName = "";
        //    if (product.ProductTemplateName == "Details")
        //    {
        //        templateName = "_Big_View" + "_Lite";
        //    }
        //    else
        //    {
        //        templateName = product.ProductTemplateName + "_Lite";
        //    }

        //    return PartialView(templateName, product);
        //}

        //[System.Web.Mvc.HttpGet]
        //[ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;expands;publishState")]
        //public override ActionResult AlternateProductImages(int id = 0, string expands = "", string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
        //{
        //    string[] expandKeys = !string.IsNullOrEmpty(expands) ? expands.Split(',') : null;
        //    ShortProductViewModel product = _productAgent.GetExtendedProductDetails(id, expandKeys);
        //    if (IsNull(product.ProductImage))
        //        return new HttpNotFoundResult("One of the supplied expands is not valid.");

        //    return PartialView("_AlternateImages", product);
        //}

        [HttpGet]
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;expands;publishState")]
        public ActionResult ProductOverview(int id = 0, string expands = "", string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
        {
            string[] expandKeys = !string.IsNullOrEmpty(expands) ? expands.Split(',') : null;
            ShortProductViewModel product = _productAgent.GetExtendedProductDetails(id, expandKeys);
            if (IsNull(product.ProductImage))
                return new HttpNotFoundResult("One of the supplied expands is not valid.");

            return PartialView("_ProductOverviewLite", product);
        }

        
        #endregion
    }
}
