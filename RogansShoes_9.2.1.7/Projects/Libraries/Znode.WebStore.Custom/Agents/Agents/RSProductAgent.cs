﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Engine.WebStore;
using Znode.Libraries.Resources;
using Znode.Engine.Api.Client.Expands;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSProductAgent:ProductAgent
    {
        #region Private Variables
        private readonly IPublishProductClient _productClient;
        #endregion

        #region Public Constructor
        public RSProductAgent(ICustomerReviewClient reviewClient, IPublishProductClient productClient, IWebStoreProductClient webstoreProductClient, ISearchClient searchClient, IHighlightClient highlightClient, IPublishCategoryClient publishCategoryClient):
            base(reviewClient,productClient,webstoreProductClient,searchClient,highlightClient,publishCategoryClient)
        {            
            _productClient = GetClient<IPublishProductClient>(productClient);            
        }
        #endregion
        public override ProductViewModel GetConfigurableProduct(ParameterProductModel model)
        {
            try
            {
                //ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE PRODUCT started."+Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                // DateTime dtt = DateTime.Now;
                ProductViewModel viewModel = base.GetConfigurableProduct(model);
                // DateTime dt = DateTime.Now;
                if (viewModel != null)
                {
                    string[] codes = model.Codes.Split(',');
                    int colorIndex = Array.IndexOf(codes, "Color");
                    string selectedcolor = model.Values.Split(',')?[colorIndex];
                    int sizeIndex = Array.IndexOf(codes, "Size");
                    string selectedsize = model.Values.Split(',')?[sizeIndex];
                    if (viewModel.ConfigurableData.SwatchImages == null)
                        viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

                    List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();

                    foreach (string color in _colors)
                    {
                      //  ZnodeLogging.LogMessage("COLOR STARTED.--" + color, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                        SwatchImageViewModel svm = new SwatchImageViewModel();
                        svm.AttributeCode = "Color";
                        svm.AttributeValues = color;
                        svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
                        viewModel.ConfigurableData.SwatchImages.Add(svm);

                        SwatchImageViewModel colorsize = new SwatchImageViewModel();
                        colorsize.AttributeCode = "ColorSize";
                        List<object> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
                        colorsize.AttributeValues = color + "-";
                        colorsize.Custom1 = color;
                        colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });

                        List<object> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == selectedcolor && Convert.ToString(x.Custom1) == selectedsize).Select(o => o.Custom2).Distinct().ToList();
                        selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });

                        viewModel.ConfigurableData.SwatchImages.Add(colorsize);
                    }
                    //TO BE UNCOMMENTED FOR PICKUP/SHIP
                    viewModel = SetDefaultStoreAddressDetails(viewModel);
                }
                // DateTime dt1 = DateTime.Now;
                //  ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE PRODUCT Finished." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return viewModel;
            }
            catch(Exception ex)
            {
                ZnodeLogging.LogMessage("RSProductAgent-Error-" + ex.Message + "- Stack Trace - " + ex.StackTrace, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
                return new ProductViewModel();
            }
        }

        public override ProductViewModel GetProduct(int productID)
        {           
            ProductViewModel viewModel = base.GetProduct(productID);
             if (viewModel != null)
            {
                string size = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Size").SelectedAttributeValue[0];
                string colorval = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Color").SelectedAttributeValue[0];
                
                if (viewModel?.ConfigurableData?.SwatchImages == null)
                    viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

                List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();
                //List<string> _colors = viewModel.AssociatedProducts.OrderBy(x => x.DisplayOrder).Select(o => o.OMSColorValue).Distinct().ToList();
                foreach (string color in _colors)
                {                   
                    SwatchImageViewModel svm = new SwatchImageViewModel();
                    svm.AttributeCode = "Color";
                    svm.AttributeValues = color;
                    svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
                    viewModel.ConfigurableData.SwatchImages.Add(svm);

                    SwatchImageViewModel colorsize = new SwatchImageViewModel();
                    colorsize.AttributeCode = "ColorSize";
                    List<object> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
                    colorsize.AttributeValues = color + "-";
                    colorsize.Custom1 = color;                   
                    colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });
                    List<object> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == colorval && Convert.ToString(x.Custom1)==size).Select(o => o.Custom2).Distinct().ToList();
                    selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });
                    viewModel.ConfigurableData.SwatchImages.Add(colorsize);
                }
                //TO BE UNCOMMENTED FOR PICKUP/SHIP
                viewModel = SetDefaultStoreAddressDetails(viewModel);
            }            
            return viewModel;
        }

        private ProductViewModel SetDefaultStoreAddressDetails(ProductViewModel viewModel)
        {
            try
            {
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                if (storeValues.Length == 3)
                {
                    viewModel.Custom1 = storeValues[0];
                    viewModel.Custom2 = storeValues[1];
                    viewModel.Custom3 = storeValues[2];
                }
            }
            catch(Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, "RSProductAgent", TraceLevel.Error, ex);
            }
            return viewModel;
        }

        private ShortProductViewModel SetDefaultStoreAddressDetails(ShortProductViewModel viewModel)
        {
            try
            {
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                if (storeValues.Length == 3)
                {
                    viewModel.Custom1 = storeValues[0];
                    viewModel.Custom2 = storeValues[1];
                    viewModel.Custom3 = storeValues[2];
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, "RSProductAgent", TraceLevel.Error, ex);
            }
            return viewModel;
        }



        /*To be deleted*/
        //public override ConfigurableAttributeViewModel GetProductAttribute(int productId, ParameterProductModel model, List<AttributesViewModel> attribute, bool isDefaultAssoicatedProduct)
        //{
        //    if (productId > 0)
        //    {
        //        ConfigurableAttributeViewModel configurableData = new ConfigurableAttributeViewModel();

        //        //Get configurable attributes.
        //        ConfigurableAttributeListModel attributes = _productClient.GetProductAttribute(productId, model);

        //        if (attributes?.Attributes?.Count > 0)
        //        {
        //            //Get the selected configurable attributes.
        //            List<AttributesViewModel> viewModel = attributes?.Attributes.ToViewModel<AttributesViewModel>().ToList();
        //            foreach (AttributesViewModel configurableAttribute in viewModel)
        //            {
        //                foreach (ProductAttributesViewModel productAttribute in configurableAttribute.ConfigurableAttribute)
        //                {
        //                    configurableAttribute.SelectedAttributeValue = new[] { (model.SelectedAttributes[configurableAttribute.AttributeCode]) };
        //                    if (isDefaultAssoicatedProduct && productAttribute.AttributeValue == model.SelectedValue)
        //                        productAttribute.IsDisabled = true;
        //                }
        //            }

        //            //Remove all configurable attributes and add newly assign configurable attributes.
        //            foreach (PublishAttributeModel configurableAttribute in attributes.Attributes)
        //                attribute.RemoveAll(x => x.AttributeCode == configurableAttribute.AttributeCode);

        //            attribute.AddRange(viewModel);

        //            configurableData.ConfigurableAttributes = attribute;

        //            return configurableData;
        //        }
        //    }
        //    return null;
        //}
        //public override ProductViewModel GetProductBrief(int productID)
        //{
        //    ProductViewModel viewModel = base.GetProductBrief(productID);
        //    if (viewModel.ConfigurableData.SwatchImages == null)
        //        viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

        //    List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();
        //    foreach (string color in _colors)
        //    {
        //        SwatchImageViewModel svm = new SwatchImageViewModel();
        //        svm.AttributeCode = "Color";
        //        svm.AttributeValues = color;
        //        svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
        //        viewModel.ConfigurableData.SwatchImages.Add(svm);
        //    }
        //    viewModel = SetDefaultStoreAddressDetails(viewModel);
        //    return viewModel;

        //}
        //public override ShortProductViewModel GetExtendedProductDetails(int productID, string[] expandKeys)
        //{
        //    ShortProductViewModel viewModel = base.GetExtendedProductDetails(productID, expandKeys);
        //    if (viewModel.ConfigurableData != null)
        //    {
        //        if (viewModel.ConfigurableData.SwatchImages == null)
        //            viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

        //        List<string> _colors = viewModel.ProductAssociations.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();
        //        foreach (string color in _colors)
        //        {
        //            SwatchImageViewModel svm = new SwatchImageViewModel();
        //            svm.AttributeCode = "Color";
        //            svm.AttributeValues = color;
        //            svm.ImagePath = viewModel.ProductAssociations.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
        //            viewModel.ConfigurableData.SwatchImages.Add(svm);
        //        }
        //    }

        //    // viewModel = SetDefaultStoreAddressDetails(viewModel);
        //    return viewModel;
        //}

        #region Performance
        //private ConfigurableAttributeViewModel GetAttribute(ParameterProductModel model, ProductViewModel product, bool isDefaultAssoicatedProduct)
        //{

        //    ConfigurableAttributeViewModel configurableData = new ConfigurableAttributeViewModel();
        //    List<AttributesViewModel> viewModel = product.Attributes.Where(x => x.AttributeCode == "Color" || x.AttributeCode == "Size" || x.AttributeCode == "ShoeWidth").ToList();
        //    foreach (AttributesViewModel configurableAttribute in viewModel)
        //    {
        //        foreach (ProductAttributesViewModel productAttribute in configurableAttribute.ConfigurableAttribute)
        //        {
        //            configurableAttribute.SelectedAttributeValue = new[] { (model.SelectedAttributes[configurableAttribute.AttributeCode]) };
        //            if (isDefaultAssoicatedProduct && productAttribute.AttributeValue == model.SelectedValue)
        //                productAttribute.IsDisabled = true;
        //        }
        //    }

        //    //Remove all configurable attributes and add newly assign configurable attributes.
        //    //foreach (PublishAttributeModel configurableAttribute in attributes.Attributes)
        //    //    attribute.RemoveAll(x => x.AttributeCode == configurableAttribute.AttributeCode);

        //    //attribute.AddRange(viewModel);

        //    configurableData.ConfigurableAttributes = viewModel;

        //    return configurableData;

        //}

        //public override ProductViewModel GetConfigurableProduct(ParameterProductModel model)
        //{
        //    // ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE PRODUCT Start." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //    if (model.ParentProductId > 0)
        //    {
        //        Dictionary<string, string> SelectedAttributes = GetAttributeValues(model.Codes, model.Values);

        //        model.LocaleId = PortalAgent.LocaleId;
        //        model.PublishCatalogId = GetCatalogId().GetValueOrDefault();
        //        model.PortalId = PortalAgent.CurrentPortal.PortalId;
        //        model.SelectedAttributes = SelectedAttributes;

        //        _productClient.SetProfileIdExplicitly(Znode.Engine.WebStore.Helper.GetProfileId().GetValueOrDefault());
        //        // ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE Service call Start." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //        PublishProductModel publishProductModel = _productClient.GetConfigurableProduct(model, GetProductExpands());
        //        // ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE Service end." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //        //PublishProductModel publishParentProductModel = _productClient.GetpublishParentProduct(model.ParentProductId, GetRequiredFilters(), GetProductExpands(false, ExpandKeys.ProductTemplate, ExpandKeys.SEO, ExpandKeys.ProductReviews, ExpandKeys.Brand));
        //        //List<PublishAttributeModel> parentPersonalizableAttributes = publishParentProductModel?.Attributes?.Where(x => x.IsPersonalizable)?.ToList();
        //        // ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE INTERnal Start." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //        if (HelperUtility.IsNotNull(model))
        //            if (HelperUtility.IsNotNull(publishProductModel))
        //            {
        //                ProductViewModel viewModel = publishProductModel.ToViewModel<ProductViewModel>();
        //                ParameterProductModel productAttribute = null;
        //                ConfigurableAttributeViewModel configurableData = null;
        //                //if (Convert.ToBoolean(!viewModel?.Attributes?.Contains(viewModel?.Attributes?.FirstOrDefault(x => x.IsPersonalizable))))
        //                //{
        //                //    foreach (PublishAttributeModel parentPersonalizableAttribute in parentPersonalizableAttributes)
        //                //    {
        //                //        viewModel.Attributes.Add(parentPersonalizableAttribute.ToViewModel<AttributesViewModel>());
        //                //    }
        //                //}
        //                //If Product is default configurable product.
        //                if (publishProductModel.IsDefaultConfigurableProduct)
        //                {
        //                    //Default product attribute.
        //                    //AttributesViewModel defaultAttribute = viewModel.Attributes?.FirstOrDefault(x => x.IsConfigurable);

        //                    productAttribute = GetConfigurableParameterModel(model.ParentProductId, model.SelectedCode, model.SelectedValue, SelectedAttributes);

        //                    //configurableData = GetProductAttribute(model.ParentProductId, productAttribute,
        //                    //                      viewModel.Attributes.Where(x => x.IsConfigurable && x.ConfigurableAttribute.Count > 0).ToList(), publishProductModel.IsDefaultConfigurableProduct);

        //                    configurableData = GetAttribute(productAttribute, viewModel, publishProductModel.IsDefaultConfigurableProduct);
        //                    //Set message id combination does not exist.
        //                    configurableData.CombinationErrorMessage = WebStore_Resources.ProductCombinationErrorMessage;
        //                    viewModel.IsDefaultConfigurableProduct = publishProductModel.IsDefaultConfigurableProduct;
        //                }
        //                else
        //                {
        //                    //Get parameter model.
        //                    productAttribute = GetConfigurableParameterModel(model.ParentProductId, model.SelectedCode, model.SelectedValue, SelectedAttributes);
        //                    //Get product aatribute values.
        //                    //configurableData = GetProductAttribute(model.ParentProductId, productAttribute,
        //                    //viewModel.Attributes.Where(x => x.IsConfigurable && x.ConfigurableAttribute.Count > 0).ToList(), publishProductModel.IsDefaultConfigurableProduct);
        //                    configurableData = GetAttribute(productAttribute, viewModel, publishProductModel.IsDefaultConfigurableProduct);
        //                }
        //                // ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE INternal end ." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //                //Map Product configurable product data.
        //                MapConfigurableProductData(model.ParentProductId, model.SKU, viewModel, configurableData);
        //                // ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE Map end." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //                Znode.Engine.WebStore.Helper.SetProductCartParameter(viewModel);
        //                // ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE Bageshree Start." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //                // return viewModel;
        //                if (viewModel != null)
        //                {
        //                    string[] codes = model.Codes.Split(',');
        //                    int colorIndex = Array.IndexOf(codes, "Color");
        //                    string selectedcolor = model.Values.Split(',')?[colorIndex];
        //                    int sizeIndex = Array.IndexOf(codes, "Size");
        //                    string selectedsize = model.Values.Split(',')?[sizeIndex];
        //                    if (viewModel.ConfigurableData.SwatchImages == null)
        //                        viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

        //                    List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();

        //                    foreach (string color in _colors)
        //                    {
        //                        // ZnodeLogging.LogMessage("color started.--" + color, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //                        SwatchImageViewModel svm = new SwatchImageViewModel();
        //                        svm.AttributeCode = "Color";
        //                        svm.AttributeValues = color;
        //                        svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
        //                        viewModel.ConfigurableData.SwatchImages.Add(svm);

        //                        SwatchImageViewModel colorsize = new SwatchImageViewModel();
        //                        colorsize.AttributeCode = "ColorSize";
        //                        List<object> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
        //                        colorsize.AttributeValues = color + "-";
        //                        colorsize.Custom1 = color;
        //                        colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });

        //                        List<object> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == selectedcolor && Convert.ToString(x.Custom1) == selectedsize).Select(o => o.Custom2).Distinct().ToList();
        //                        selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });

        //                        viewModel.ConfigurableData.SwatchImages.Add(colorsize);
        //                    }
        //                    //TO BE UNCOMMENTED FOR PICKUP/SHIP
        //                    viewModel = SetDefaultStoreAddressDetails(viewModel);
        //                }
        //                // ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE Bageshree code end." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //                // DateTime dt1 = DateTime.Now;
        //                //    ZnodeLogging.LogMessage("RSPRODUCTAGENT-GETCONFIGURABLE PRODUCT Finished." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //                return viewModel;
        //            }
        //    }
        //    return new ProductViewModel { Attributes = new List<AttributesViewModel>() };
        //}

        //private ParameterProductModel GetConfigurableParameterModel(int productId, string selectedCode, string selectedValue, Dictionary<string, string> SelectedAttributes)
        //{
        //    ParameterProductModel productAttribute = new ParameterProductModel();
        //    productAttribute.ParentProductId = productId;
        //    productAttribute.LocaleId = PortalAgent.LocaleId;
        //    productAttribute.SelectedAttributes = SelectedAttributes;
        //    productAttribute.PortalId = PortalAgent.CurrentPortal.PortalId;
        //    productAttribute.SelectedCode = selectedCode;
        //    productAttribute.SelectedValue = selectedValue;
        //    return productAttribute;
        //}

        #endregion
    }
}
