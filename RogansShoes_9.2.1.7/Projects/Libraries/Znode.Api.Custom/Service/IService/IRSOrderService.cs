﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;

namespace Znode.Api.Custom.Service.IService
{
    public interface IRSOrderService : IOrderService
    {
        bool ExecutePackage();
        bool UpdateOrderStatusMany(OrderLineItemDataListModel orderDetailListModel);
    }
}
