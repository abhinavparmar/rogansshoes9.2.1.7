﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.Service.Service
{
    public class RSPublishProductService : PublishProductService, IRSPublishProductService
    {
        private readonly IMongoRepository<ConfigurableProductEntity> _configurableproductRepository;
        private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;
        private readonly IZnodeRepository<ZnodePublishProduct> _publishProductRepository;
        private readonly IPublishProductHelper publishProductHelper;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly ISEOService _seoService;


        #region Constructor
        public RSPublishProductService() : base()
        {
            _ProductMongoRepository = new MongoRepository<ProductEntity>();
            _configurableproductRepository = new MongoRepository<ConfigurableProductEntity>();
            _publishProductRepository = new ZnodeRepository<ZnodePublishProduct>();
            publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();
            _categoryMongoRepository = new MongoRepository<CategoryEntity>(GetCatalogVersionId());
            _seoService = new SEOService();

        }
        #endregion
        public override PublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
          //  ZnodeLogging.LogMessage("RSPublishProductService-GetPublishProduct Started." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            GetParametersValueForFilters(filters, out int catalogId, out int portalId, out int localeId);
            int versionId = GetCatalogVersionId(catalogId, localeId).GetValueOrDefault();

            PublishProductModel publishProduct = base.GetPublishProduct(publishProductId, filters, expands);
             return publishProduct;
        }

        public override PublishProductModel GetConfigurableProduct(ParameterProductModel productAttributes, NameValueCollection expands)
        {
            try
            {
                // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE PRODUCT started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                PublishProductModel product = null;

                IMongoQuery query = Query<ConfigurableProductEntity>.EQ(pr => pr.ZnodeProductId, productAttributes.ParentProductId);
                int? versionId = GetCatalogVersionId(productAttributes.PublishCatalogId);

                query = Query.And(query, Query<ConfigurableProductEntity>.EQ(x => x.VersionId, versionId));
                // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE config Entity  started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                List<ConfigurableProductEntity> configEntity = _configurableproductRepository.GetEntityList(query, true);

                FilterCollection filters = GetConfigurableProductFilter(productAttributes.LocaleId, configEntity);
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, versionId.ToString());

                List<IMongoQuery> mongoQuery;

                // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE PRODUCTMongo started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                List<ProductEntity> productList = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), true);
                List<ProductEntity> associatedProducts = productList;


                // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE for loop started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                foreach (var item in productAttributes.SelectedAttributes)
                {
                    List<ProductEntity> _newProductList = new List<ProductEntity>();
                    productList.ForEach(x =>
                    {
                        AttributeEntity _Attributes = x.Attributes.FirstOrDefault(y => y.AttributeCode == item.Key && y.SelectValues.FirstOrDefault()?.Value == item.Value);

                        if (HelperUtility.IsNotNull(_Attributes))
                            _newProductList.Add(x);
                    });
                    productList = _newProductList;
                }
                //  ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE loop end ." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                ProductEntity productEntity = productList.FirstOrDefault();

                //If Combination does not exist.
                if (HelperUtility.IsNull(productEntity))
                {
                    //Get filter
                    // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE INEtrnal started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    filters = GetConfigurableProductFilter(productAttributes.LocaleId, configEntity);
                    productAttributes.SelectedAttributes = new Dictionary<string, string>();
                    mongoQuery = GetMongoQueryForConfigurableProduct(productAttributes, filters);

                    //Get Default product.
                    productEntity = _ProductMongoRepository.GetEntity(Query.And(mongoQuery));
                    product = productEntity?.ToModel<PublishProductModel>();
                    //Set IsDefaultConfigurableProduct flag.
                    if (HelperUtility.IsNotNull(product))
                        product.IsDefaultConfigurableProduct = true;
                    // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE INRERNal end started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                }
                else
                    product = productEntity?.ToModel<PublishProductModel>();

                if (HelperUtility.IsNotNull(product))
                {
                    //  ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE PRODUCT Inetrnal not started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    product.AssociatedGroupProducts = productList.ToModel<WebStoreGroupProductModel>().ToList();

                    product.ConfigurableProductId = productAttributes.ParentProductId;
                    product.IsConfigurableProduct = true;

                    product.ProductType = ZnodeConstant.ConfigurableProduct;
                    product.ConfigurableProductSKU = product.SKU;
                    product.SKU = productAttributes.ParentProductSKU;

                    publishProductHelper.GetDataFromExpands(productAttributes.PortalId, GetExpands(expands), product, productAttributes.LocaleId, string.Empty, GetLoginUserId(), null, null, GetProfileId());
                    // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE GetImage started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    GetProductImagePath(productAttributes.PortalId, product);
                    //set stored based In Stock, Out Of Stock, Back Order Message.
                    SetPortalBasedDetails(productAttributes.PortalId, product);
                    //  ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE SetPortalBased end." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    /*Nivi Code*/
                    //  ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE Nivi started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    string _swatchImagePath = "";
                    int index = 0;
                    if (product.ImageThumbNailPath == null)
                    {
                        ImageHelper image = new ImageHelper(productAttributes.PortalId);
                        string productImage = image.GetImageHttpPathThumbnail("image"); ;
                        index = productImage.LastIndexOf('/');
                        if (index != -1)
                        {
                            _swatchImagePath = productImage.Substring(0, index) + "/";
                        }
                    }
                    else
                    {
                        index = product.ImageThumbNailPath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _swatchImagePath = product.ImageThumbNailPath.Substring(0, index) + "/";
                        }
                    }
                    List<AssociatedProductsModel> _products = (from p in associatedProducts

                                                               select new AssociatedProductsModel
                                                               {
                                                                   PublishProductId = p.ZnodeProductId,
                                                                   //PimProductId = pimProducts.FirstOrDefault(w => w.PublishProductId == p.ZnodeProductId)?.PimProductId,
                                                                   SKU = p.SKU,
                                                                   OMSColorCode = p.Attributes.Where(x => x.AttributeCode == ZnodeConstant.GalleryImages)?.FirstOrDefault()?.AttributeValues,
                                                                   OMSColorValue = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code,
                                                                   OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
                                                                   DisplayOrder = p.DisplayOrder,
                                                                   OMSColorSwatchText = p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
                                                                   Custom1 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code,
                                                                   Custom2 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code
                                                               }).ToList();
                    product.AssociatedProducts = new List<AssociatedProductsModel>();
                    product.AssociatedProducts.AddRange(_products);
                    string[] codes = productAttributes.Codes.Split(',');
                    int colorIndex = Array.IndexOf(codes, "Color");
                    string selectedcolor = productAttributes.Values.Split(',')?[colorIndex];
                    string prdImage = product.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == selectedcolor)?.OMSColorSwatchText;
                    ZnodeLogging.LogMessage("prdImage for selected color=" + prdImage, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
                    ZnodeLogging.LogMessage("prdImage for product from base=" + product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
                    if (prdImage != product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues)
                        GetProductImagesforUnavailableProduct(product, productAttributes.PortalId, selectedcolor);
                    //   ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE Nivi ENd." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    /*Nivi Code*/
                }
                return product;
            }
            catch(Exception ex)
            {
                ZnodeLogging.LogMessage("RSPublishProductService-Error-" + ex.Message +"- Stack Trace - "+ex.StackTrace, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
                return new PublishProductModel();
            }
           
           //ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE-GETCONFIGURABLE PRODUCT FINISHED." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
           
            
        }
        
        protected override void GetProductAlterNateImages(PublishProductModel product, int portalId, IImageHelper imageHelper)
        {
            string alternateImages = product.Attributes.Where(x => x.AttributeCode == ZnodeConstant.GalleryImages)?.FirstOrDefault()?.AttributeValues;
            if (!string.IsNullOrEmpty(alternateImages))
            {
                List<ProductAlterNateImageModel> mediaAttributeList = GetGalleryImageMediaAttributes(alternateImages);
                // List<ProductAlterNateImageModel> mediaAttributeList = GetList(alternateImages);
                product.AlternateImages = new List<ProductAlterNateImageModel>();
                string[] images = alternateImages.Split(',');
                foreach (string image in images)
                {
                    string imageAltText = mediaAttributeList?.FirstOrDefault(x => x.FileName == image && x.ImageSmallThumbNail == "Display Name")?.ImageSmallPath;
                    string displayOrder = mediaAttributeList?.FirstOrDefault(x => x.FileName == image && x.ImageSmallThumbNail == "ImageDisplayOrderNumber")?.ImageSmallPath;
                    product.AlternateImages.Add(new ProductAlterNateImageModel { FileName = image, ImageSmallPath = imageAltText, ImageThumbNailPath = displayOrder, OriginalImagePath = imageHelper.GetOriginalImagepath(image), ImageLargePath = imageHelper.GetImageHttpPathLarge(image) });
                    //product.AlternateImages.Add(new ProductAlterNateImageModel { FileName = image, ImageSmallPath = imageHelper.GetImageHttpPathSmall(image), ImageThumbNailPath = imageHelper.GetImageHttpPathThumbnail(image), OriginalImagePath = imageHelper.GetOriginalImagepath(image), ImageLargePath = imageHelper.GetImageHttpPathLarge(image) });
                }
            }
        }

        public void GetProductImagesforUnavailableProduct(PublishProductModel product, int portalId,string color)
        {
           //ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE-GETPRODUCTIMAGESFORUNAVAILABLEPRODUCT STARTED." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            string prdImage = product.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorSwatchText;
            ImageHelper imageHelper = new ImageHelper(portalId);          
            product.ImageLargePath = imageHelper.GetImageHttpPathLarge(prdImage);
            product.ImageMediumPath = imageHelper.GetImageHttpPathMedium(prdImage);
            product.ImageThumbNailPath = imageHelper.GetImageHttpPathThumbnail(prdImage);
            product.ImageSmallPath = imageHelper.GetImageHttpPathSmall(prdImage);
            product.OriginalImagepath = imageHelper.GetOriginalImagepath(prdImage);
            string alternateImages = product.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorCode;
            if (!string.IsNullOrEmpty(alternateImages))
            {
                List<ProductAlterNateImageModel> mediaAttributeList = GetGalleryImageMediaAttributes(alternateImages);               
                product.AlternateImages = new List<ProductAlterNateImageModel>();
                string[] images = alternateImages.Split(',');
                foreach (string image1 in images)
                {
                    string imageAltText = mediaAttributeList?.FirstOrDefault(x => x.FileName == image1 && x.ImageSmallThumbNail == "Display Name")?.ImageSmallPath;
                    string displayOrder = mediaAttributeList?.FirstOrDefault(x => x.FileName == image1 && x.ImageSmallThumbNail == "ImageDisplayOrderNumber")?.ImageSmallPath;
                    product.AlternateImages.Add(new ProductAlterNateImageModel { FileName = image1, ImageSmallPath = imageAltText, ImageThumbNailPath = displayOrder, OriginalImagePath = imageHelper.GetOriginalImagepath(image1), ImageLargePath = imageHelper.GetImageHttpPathLarge(image1) });
                    //product.AlternateImages.Add(new ProductAlterNateImageModel { FileName = image, ImageSmallPath = imageHelper.GetImageHttpPathSmall(image), ImageThumbNailPath = imageHelper.GetImageHttpPathThumbnail(image), OriginalImagePath = imageHelper.GetOriginalImagepath(image), ImageLargePath = imageHelper.GetImageHttpPathLarge(image) });
                }
            }
            //ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE-GETPRODUCTIMAGESFORUNAVAILABLEPRODUCT FINISHED." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        }

        private List<ProductAlterNateImageModel> GetGalleryImageMediaAttributes(string alternateImages)
        {
            IZnodeViewRepository<ProductAlterNateImageModel> objStoredProc = new ZnodeViewRepository<ProductAlterNateImageModel>();
            objStoredProc.SetParameter("@media", alternateImages, ParameterDirection.Input, DbType.String);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("RS_GetGalleryImageMediaAttributes @media").ToList();
        }         

        public DataTable GetStoreLocations(string state, string sku)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@State", state, ParameterDirection.Input, SqlDbType.VarChar);
            objStoredProc.GetParameter("@SKU", sku, ParameterDirection.Input, SqlDbType.VarChar);

            DataSet data = objStoredProc.GetSPResultInDataSet("RS_GetStoreAddressDetails");

            DataTable storeLocationTable = data.Tables[0];

            return storeLocationTable;
        }

        public DataTable GetStoreLocationDetails(string state, string sku)
        {
            DataTable storeData = this.GetStoreLocations(state, sku);
            return storeData;
        }      

        protected override PublishProductModel GetDefaultConfiurableProduct(NameValueCollection expands, int portalId, int localeId, PublishProductModel publishProduct, List<ProductEntity> associatedProducts, List<string> ConfigurableAttributeCodes = null, int? catalogVersionId = 0)
        {
            
            //ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE-GETDEFAULTCONFIURABLEPRODUCT STARTED." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
           
            string sku = publishProduct.SKU;
            string parentSEOCode = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "SKU")?.AttributeValues;
            int categoryIds = publishProduct.ZnodeCategoryIds;
            List<PublishCategoryModel> categoryHierarchyIds = publishProduct.CategoryHierarchy;

            List<PublishAttributeModel> parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();
            string parentConfigurableProductName = publishProduct.Name;
            int configurableProductId = publishProduct.PublishProductId;
           
            List<ProductEntity> newassociatedProducts = new List<ProductEntity>();
            foreach (ProductEntity product in associatedProducts)
            {
                foreach (AttributeEntity attributeEntity in product.Attributes)
                {
                    if (attributeEntity.IsConfigurable)
                    {
                        attributeEntity.AttributeValues = attributeEntity.SelectValues.FirstOrDefault()?.Value;
                        /*NIVI CODE*/
                        if (attributeEntity.AttributeCode == "Color")
                        {
                            //assigned display order of product to configurable attribute to display it on webstore depend on display order.
                            if (attributeEntity?.SelectValues?.Count > 0)
                                attributeEntity.SelectValues.FirstOrDefault().DisplayOrder = product?.DisplayOrder;

                            attributeEntity.DisplayOrder = product.DisplayOrder;
                        }
                        else
                        {

                            if (attributeEntity?.SelectValues?.Count > 0)
                            {
                                //attributeEntity.SelectValues.FirstOrDefault().DisplayOrder = product?.DisplayOrder;
                                attributeEntity.DisplayOrder = Convert.ToInt32(attributeEntity.SelectValues.FirstOrDefault().DisplayOrder);
                            }
                        }
                        /*NIVI CODE*/
                    }
                }
                newassociatedProducts.Add(product);
            }
          
            //Get first product from list of associated products 
            publishProduct = newassociatedProducts.FirstOrDefault().ToModel<PublishProductModel>();

            publishProduct.ConfigurableProductId = configurableProductId;
            publishProduct.ParentSEOCode = parentSEOCode;
            publishProduct.ConfigurableProductSKU = publishProduct.SKU;
            publishProduct.CategoryHierarchy = categoryHierarchyIds;

            publishProduct.SKU = sku;
            publishProduct.ZnodeCategoryIds = categoryIds;
            publishProduct.IsConfigurableProduct = true;
            publishProduct.ParentConfiguarableProductName = parentConfigurableProductName;

            catalogVersionId = Convert.ToInt32(catalogVersionId) > 0 ? catalogVersionId : GetCatalogVersionId();
            
            publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId, WebstoreVersionId, GetProfileId());

            PublishAttributeModel defaultAttribute = publishProduct.Attributes?.FirstOrDefault(x => x.IsConfigurable);
            bool isChildPersonalizableAttribute = Convert.ToBoolean(publishProduct.Attributes?.Contains(publishProduct.Attributes?.FirstOrDefault(x => x.IsPersonalizable)));

            List<PublishAttributeModel> variants = publishProduct?.Attributes?.Where(x => x.IsConfigurable).ToList();
            Dictionary<string, string> selectedAttribute = GetSelectedAttributes(variants);

            List<PublishAttributeModel> attributeList = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(newassociatedProducts, ConfigurableAttributeCodes), defaultAttribute?.AttributeCode, defaultAttribute?.AttributeValues, selectedAttribute, newassociatedProducts, ConfigurableAttributeCodes, portalId);
           
            foreach (PublishAttributeModel item in attributeList)
                publishProduct.Attributes.RemoveAll(x => x.AttributeCode == item.AttributeCode);

            publishProduct.Attributes.AddRange(attributeList);
            /*NIVI CODE*/
            string _swatchImagePath = "";
            int index = 0;
            if (publishProduct.ImageThumbNailPath == null)
            {
                ImageHelper image = new ImageHelper(portalId);
                string productImage = image.GetImageHttpPathThumbnail("image"); ;
                index = productImage.LastIndexOf('/');
                if (index != -1)
                {
                    _swatchImagePath = productImage.Substring(0, index) + "/";
                }
            }
            else
            {
                index = publishProduct.ImageThumbNailPath.LastIndexOf('/');
                if (index != -1)
                {
                    _swatchImagePath = publishProduct.ImageThumbNailPath.Substring(0, index) + "/";
                }
            }
            List<AssociatedProductsModel> _products = (from p in associatedProducts

                                                       select new AssociatedProductsModel
                                                       {
                                                           PublishProductId = p.ZnodeProductId,
                                                           //PimProductId = pimProducts.FirstOrDefault(w => w.PublishProductId == p.ZnodeProductId)?.PimProductId,
                                                           SKU = p.SKU,
                                                           OMSColorCode = p.Attributes.Where(x => x.AttributeCode == ZnodeConstant.GalleryImages)?.FirstOrDefault()?.AttributeValues,
                                                           OMSColorValue = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code,
                                                           OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
                                                           DisplayOrder = p.DisplayOrder,
                                                           OMSColorSwatchText = p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
                                                           Custom1 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code,
                                                           Custom2 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code
                                                       }).ToList();
            publishProduct.AssociatedProducts = new List<AssociatedProductsModel>();
            publishProduct.AssociatedProducts.AddRange(_products);
            /*NIVI CODE*/
            return AddPersonalizeAttributeInChildProduct(publishProduct, parentPersonalizableAttributes, isChildPersonalizableAttribute);
           
        }

        private PublishProductModel AddPersonalizeAttributeInChildProduct(PublishProductModel publishProduct, List<PublishAttributeModel> parentPersonalizableAttributes, bool isChildPersonalizableAttribute)
        {
            if (!isChildPersonalizableAttribute)
            {
                if (parentPersonalizableAttributes?.Count > 0)
                {
                    foreach (PublishAttributeModel parentPersonalizableAttribute in parentPersonalizableAttributes)
                    {
                        publishProduct.Attributes.Add(parentPersonalizableAttribute);
                    }
                }
            }
           // ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE-GETDEFAULTCONFIURABLEPRODUCT FINISHED." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            return publishProduct;
        }

        protected override void GetProductImagePath(int portalId, PublishProductModel publishProduct, bool includeAlternateImages = true)
        {

            //Get Product Image Path
            if (portalId > 0 && HelperUtility.IsNotNull(publishProduct))
            {
                ImageHelper image = new ImageHelper(portalId);
                string ProductImageName = publishProduct.Attributes.Where(x => x.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues;
                publishProduct.ImageLargePath = image.GetImageHttpPathLarge(ProductImageName);
                publishProduct.ImageMediumPath = image.GetImageHttpPathMedium(ProductImageName);
                publishProduct.ImageThumbNailPath = image.GetImageHttpPathThumbnail(ProductImageName);
                publishProduct.ImageSmallPath = image.GetImageHttpPathSmall(ProductImageName);
                publishProduct.OriginalImagepath = image.GetOriginalImagepath(ProductImageName);
                //if (includeAlternateImages)
                GetProductAlterNateImages(publishProduct, portalId, image);
            }
        }





        #region To be deleted

        //public override PublishProductModel GetPublishProductBrief(int publishProductId, FilterCollection filters, NameValueCollection expands)
        //{
        //    GetParametersValueForFilters(filters, out int catalogId, out int portalId, out int localeId);
        //    int versionId = GetCatalogVersionId(catalogId, localeId).GetValueOrDefault();

        //    PublishProductModel publishProduct = base.GetPublishProductBrief(publishProductId, filters, expands);
        //    //GetAssociatedProducts(publishProduct, portalId, GetLoginUserId(), versionId);
        //    if (publishProduct.ProductTemplateName == null)
        //    {
        //        publishProduct.ProductTemplateName = "_Big_View";
        //    }

        //    return publishProduct;
        //}

        //public override PublishProductModel GetExtendedPublishProductDetails(int publishProductId, FilterCollection filters, NameValueCollection expands)
        //{
        //    GetParametersValueForFilters(filters, out int catalogId, out int portalId, out int localeId);
        //    int versionId = GetCatalogVersionId(catalogId, localeId).GetValueOrDefault();

        //    PublishProductModel publishProduct = base.GetExtendedPublishProductDetails(publishProductId, filters, expands);
        //   // GetAssociatedProducts(publishProduct, portalId, GetLoginUserId(), versionId);
        //    return publishProduct;


        //}
        //private PublishProductModel GetPublishedProductFromMongo(int publishProductId, FilterCollection filters)
        //{
        //    //Get parameter values from filters.
        //    int catalogId, portalId, localeId;
        //    GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

        //    //Remove portal id filter.
        //    filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

        //    //Replace filter keys.
        //    ReplaceFilterKeys(ref filters);

        //    //get catalog current version id by catalog id.
        //    int? catalogVersionId = GetCatalogVersionId(catalogId);

        //    filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, Convert.ToString(publishProductId));

        //    if (catalogVersionId > 0)
        //        filters.Add(FilterKeys.VersionId, FilterOperators.Equals, catalogVersionId.HasValue ? catalogVersionId.Value.ToString() : "0");

        //    PublishProductModel publishProduct = null;
        //    CategoryEntity associatedCategory = null;
        //    //Get publish product from mongo
        //    List<ProductEntity> products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), false);

        //    if (HelperUtility.IsNotNull(products))
        //    {
        //        associatedCategory = GetActiveAssociatedCategory(products);
        //        if (HelperUtility.IsNotNull(associatedCategory))
        //        {
        //            publishProduct = products?.FirstOrDefault(x => x.ZnodeCategoryIds == associatedCategory.ZnodeCategoryId).ToModel<PublishProductModel>();
        //            publishProduct.CategoryHierarchy = GetProductCategory(associatedCategory.ZnodeCategoryId.ToString(), localeId, catalogId, _seoService.GetPublishSEOSettingList(ZnodeConstant.Category, portalId, localeId));
        //        }
        //    }

        //    return publishProduct;
        //}
        //private bool ContainsExpandables(NameValueCollection expands)
        //{
        //    string[] expandKeys = expands.AllKeys;
        //    string[] expandableKeys = new string[]
        //    {
        //        ZnodeConstant.Promotions.ToLowerInvariant(),
        //        ZnodeConstant.SEO.ToLowerInvariant(),
        //        ZnodeConstant.Inventory.ToLowerInvariant(),
        //        ZnodeConstant.Pricing.ToLowerInvariant(),
        //        ZnodeConstant.AddOns.ToLowerInvariant(),
        //        ZnodeConstant.ProductBrand.ToLowerInvariant(),
        //        ZnodeConstant.ProductReviews.ToLowerInvariant(),
        //        ZnodeConstant.AssociatedProducts.ToLowerInvariant(),
        //        ZnodeConstant.ProductTemplate.ToLowerInvariant()
        //    };

        //    return expandKeys.Intersect(expandableKeys).Count() > 0;
        //}
        //private CategoryEntity GetActiveAssociatedCategory(List<ProductEntity> products)
        //{
        //    List<int> categoryIds = products.Select(x => x.ZnodeCategoryIds).ToList();
        //    IMongoQuery categoryQuery = Query.And(
        //        Query<CategoryEntity>.In(x => x.ZnodeCategoryId, categoryIds),
        //        Query<CategoryEntity>.EQ(x => x.IsActive, true));

        //    return _categoryMongoRepository.GetEntity(categoryQuery);
        //}

        //public override ConfigurableAttributeListModel GetProductAttribute(int productId, ParameterProductModel model)
        //{
        //     if (productId > 0)
        //    {
        //        //Get Configurable product entity.
        //        List<ConfigurableProductEntity> configEntiy = publishProductHelper.GetConfigurableProductEntity(productId, GetCatalogVersionId(model.PublishCatalogId, model.LocaleId));

        //        //Get associated configurable product list.
        //        List<ProductEntity> products = publishProductHelper.GetAssociatedProducts(productId, model.LocaleId, GetCatalogVersionId(model.PublishCatalogId, model.LocaleId), configEntiy);

        //        return new ConfigurableAttributeListModel()
        //        {
        //            //Get associated configurable product Attribute list.
        //            Attributes = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(products, configEntiy?.FirstOrDefault()?.ConfigurableAttributeCodes), model.SelectedCode, model.SelectedValue, model.SelectedAttributes, products, configEntiy?.FirstOrDefault()?.ConfigurableAttributeCodes, model.PortalId),
        //        };
        //    }
        //    ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
        //    return new ConfigurableAttributeListModel();
        //}
        //protected override List<PublishAttributeModel> MapWebStoreConfigurableAttributeData(List<List<AttributeEntity>> attributeList, string selectedCode, string selectedValue, Dictionary<string, string> SelectedAttributes, List<ProductEntity> products, List<string> ConfigurableAttributeCodes, int portalId)
        //{
        //    List<PublishAttributeModel> configurableAttributeList = new List<PublishAttributeModel>();
        //    List<ConfigurableAttributeModel> attributesList = new List<ConfigurableAttributeModel>();
        //    // ImageHelper image = GetService<ImageHelper>(new ZnodeNamedParameter("PortalId", portalId));
        //    ImageHelper image = new ImageHelper(portalId);


        //    ZnodeLogging.LogMessage($"MapWebStoreConfigurableAttributeData - Request selected attribute count {SelectedAttributes.Count}, selectedCode {selectedCode}, selectedValue {selectedValue}", "PublishProductAttrMapping", TraceLevel.Info);

        //    if (SelectedAttributes.Count <= 0 && !string.IsNullOrEmpty(attributeList.FirstOrDefault().FirstOrDefault().AttributeCode) && !string.IsNullOrEmpty(attributeList.FirstOrDefault().FirstOrDefault().AttributeValues))
        //        SelectedAttributes.Add(attributeList.FirstOrDefault().FirstOrDefault().AttributeCode, attributeList.FirstOrDefault().FirstOrDefault().AttributeValues);

        //    IEnumerable<AttributeEntity> attributes = ExitingAttributeList(selectedCode, selectedValue, products, SelectedAttributes);

        //    var matchAttributeList = attributes.GroupBy(w => w.AttributeCode).Select(g => new
        //    {
        //        AttributeCode = g.Key,
        //        AttributeValues = g.Select(c => c.AttributeValues)
        //    });

        //    foreach (List<AttributeEntity> attributeEntityList in attributeList)
        //    {
        //        attributesList.Clear();
        //        PublishAttributeModel attributesModel = new PublishAttributeModel();
        //        foreach (AttributeEntity attributeValue in attributeEntityList)
        //        {
        //            //Check if attribute already exist in list.
        //            if (!AlreadyExist(attributesList, attributeValue.AttributeValues).GetValueOrDefault())
        //            {
        //                IEnumerable<string> matchAttribute = matchAttributeList?.FirstOrDefault(x => x.AttributeCode == attributeValue.AttributeCode)?.AttributeValues;

        //                ConfigurableAttributeModel attribute = new ConfigurableAttributeModel();

        //                if (HelperUtility.IsNotNull(matchAttribute) && !matchAttribute.Contains(attributeValue.AttributeValues) && ConfigurableAttributeCodes?.Count != 1)
        //                    attribute.IsDisabled = true;

        //                attribute.AttributeValue = attributeValue.AttributeValues;

        //                //Assign the display order value of configurable type attributes.
        //                attribute.SelectValues = attributeValue.SelectValues?.ToModel<AttributesSelectValuesModel>().ToList();

        //                if (attribute.SelectValues?.Count > 0)
        //                {
        //                    AttributesSelectValuesModel selectValues = attribute.SelectValues.FirstOrDefault();

        //                    if (string.Equals(attributeValue.IsSwatch, ZnodeConstant.TrueValue, StringComparison.InvariantCultureIgnoreCase))
        //                        attribute.ImagePath = image.GetImageHttpPathSmallThumbnail(selectValues.Path);

        //                    attribute.SwatchText = selectValues.SwatchText;
        //                }

        //                attribute.DisplayOrder = attributes.Where(x => x.AttributeValues == attributeValue.AttributeValues).Min(x => x?.DisplayOrder) ?? attributeValue.DisplayOrder;
        //                attributesList.Add(attribute);
        //            }
        //        }
        //        ZnodeLogging.LogMessage("attributesList list count:", string.Empty, TraceLevel.Verbose, attributesList?.Count());

        //        AttributeEntity attributeEntity = attributeEntityList.FirstOrDefault();
        //        attributesModel.AttributeName = attributeEntity.AttributeName;
        //        attributesModel.AttributeCode = attributeEntity.AttributeCode;
        //        attributesModel.AttributeValues = attributeEntity.AttributeValues;
        //        attributesModel.IsConfigurable = attributeEntity.IsConfigurable;
        //        attributesModel.IsSwatch = attributeEntity.IsSwatch;
        //        attributesModel.SelectValues = attributeEntity.SelectValues?.ToModel<AttributesSelectValuesModel>().ToList();
        //        attributesModel.ConfigurableAttribute.AddRange(attributesList);
        //        configurableAttributeList.Add(attributesModel);
        //    }
        //    return configurableAttributeList;
        //}

        //public override PublishProductListModel GetPublishProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        //{
        //    try
        //    {
        //        return base.GetPublishProductList(expands, filters, sorts, page);
        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage("GetPublishProductList erorr" + ex.Message, string.Empty, TraceLevel.Error);
        //        throw ex;
        //    }
        //}
        //public void GetAssociatedProducts(PublishProductModel publishProduct, int portalId, int userId, int? catalogVersionId = null)
        //{
        //    //PublishProductModel config = base.GetAssociatedConfigurableProduct(publishProduct.ConfigurableProductId, publishProduct.LocaleId, catalogVersionId, portalId);
        //    ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE-GETASSOCIATEDPRODUCTS STARTED." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //    try
        //    {
        //        IMongoQuery query = Query<ConfigurableProductEntity>.EQ(pr => pr.ZnodeProductId, publishProduct.ConfigurableProductId);
        //        if (catalogVersionId.HasValue && catalogVersionId.Value > 0)
        //        {
        //            query = Query.And(query, Query<ConfigurableProductEntity>.EQ(pr => pr.VersionId, catalogVersionId));
        //        }

        //        publishProduct.AssociatedProducts = new List<AssociatedProductsModel>();

        //        PublishProductHelper publishProductHelper = new PublishProductHelper();
        //        //Get Configurable Product
        //        //
        //        List<ConfigurableProductEntity> configEntity = _configurableproductRepository.GetEntityList(query); 
        //       // ZnodeLogging.LogMessage("publishProduct.PublishProductId="+ Convert.ToString(publishProduct.PublishProductId), string.Empty, TraceLevel.Error); 
        //        if (!Equals(configEntity, null) && configEntity?.Count > 0)
        //        {
        //             List<ProductEntity> products = publishProductHelper.GetAssociatedProducts(publishProduct.PublishProductId, 1, catalogVersionId, configEntity);


        //           // FilterDataCollection fdata = new FilterDataCollection
        //           //{
        //           //    { "PublishProductId", FilterOperators.In, string.Join(",", products.Select(x => x.ZnodeProductId).ToArray()) }
        //           //};
        //           // IList<ZnodePublishProduct> pimProducts = _publishProductRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClause(fdata));
        //            string _swatchImagePath = "";
        //            int index = 0;
        //            if (publishProduct.ImageThumbNailPath == null)
        //            {
        //                ImageHelper image = new ImageHelper(portalId);
        //                string productImage = image.GetImageHttpPathThumbnail("image"); ;
        //                index = productImage.LastIndexOf('/');
        //                if (index != -1)
        //                {
        //                    _swatchImagePath = productImage.Substring(0, index) + "/";
        //                }
        //            }
        //            else
        //            {
        //                index = publishProduct.ImageThumbNailPath.LastIndexOf('/');
        //                if (index != -1)
        //                {
        //                    _swatchImagePath = publishProduct.ImageThumbNailPath.Substring(0, index) + "/";
        //                }
        //            }



        //            List<AssociatedProductsModel> _products = (from p in products

        //                                                       select new AssociatedProductsModel
        //                                                       {
        //                                                           PublishProductId = p.ZnodeProductId,
        //                                                           //PimProductId = pimProducts.FirstOrDefault(w => w.PublishProductId == p.ZnodeProductId)?.PimProductId,
        //                                                           SKU = p.SKU,
        //                                                           OMSColorCode = p.Attributes.Where(x => x.AttributeCode == ZnodeConstant.GalleryImages)?.FirstOrDefault()?.AttributeValues,
        //                                                           OMSColorValue = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code,
        //                                                           OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
        //                                                           DisplayOrder = p.DisplayOrder,
        //                                                           OMSColorSwatchText = p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
        //                                                           Custom1 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code,
        //                                                           Custom2 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code                                                                 
        //                                                       }).ToList();

        //            publishProduct.AssociatedProducts.AddRange(_products);
        //        }
        //       // ZnodeLogging.LogMessage("Out configEntity", string.Empty, TraceLevel.Error);
        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage(ex, ex.StackTrace, TraceLevel.Error);
        //    }
        //    ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE-GETASSOCIATEDPRODUCTS FINISHED." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //}
        #endregion

        #region Performance Changes

        //public override PublishProductModel GetConfigurableProduct(ParameterProductModel productAttributes, NameValueCollection expands)
        //{
        //    // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE PRODUCT started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //    PublishProductModel product = null;

        //    IMongoQuery query = Query<ConfigurableProductEntity>.EQ(pr => pr.ZnodeProductId, productAttributes.ParentProductId);
        //    int? versionId = GetCatalogVersionId(productAttributes.PublishCatalogId);

        //    query = Query.And(query, Query<ConfigurableProductEntity>.EQ(x => x.VersionId, versionId));
        //    // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE config Entity  started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //    List<ConfigurableProductEntity> configEntity = _configurableproductRepository.GetEntityList(query, true);

        //    FilterCollection filters = GetConfigurableProductFilter(productAttributes.LocaleId, configEntity);
        //    filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, versionId.ToString());

        //    List<IMongoQuery> mongoQuery;

        //    // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE PRODUCTMongo started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //    List<ProductEntity> productList = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), true);
        //    List<ProductEntity> associatedProducts = productList;
        //    /*NIVI CODE*/
        //    productList.ForEach(x =>
        //    {
        //        x.Attributes.Where(y => y.AttributeCode == "Color").ToList().ForEach(cc => cc.AttributeValues = cc.SelectValues.FirstOrDefault()?.Value);
        //        x.Attributes.Where(y => y.AttributeCode == "Size").ToList().ForEach(cc => cc.AttributeValues = cc.SelectValues.FirstOrDefault()?.Value);
        //        x.Attributes.Where(y => y.AttributeCode == "ShoeWidth").ToList().ForEach(cc => cc.AttributeValues = cc.SelectValues.FirstOrDefault()?.Value);
        //        x.Attributes.Where(y => y.AttributeCode == "Color").ToList().ForEach(cc => cc.DisplayOrder = Convert.ToInt32(cc.SelectValues.FirstOrDefault()?.DisplayOrder));
        //        x.Attributes.Where(y => y.AttributeCode == "Size").ToList().ForEach(cc => cc.DisplayOrder = Convert.ToInt32(cc.SelectValues.FirstOrDefault()?.DisplayOrder));
        //        x.Attributes.Where(y => y.AttributeCode == "ShoeWidth").ToList().ForEach(cc => cc.DisplayOrder = Convert.ToInt32(cc.SelectValues.FirstOrDefault()?.DisplayOrder));

        //    });

        //    ConfigurableAttributeListModel configurableAttributeListModel = new ConfigurableAttributeListModel()
        //    {
        //        //Get associated configurable product Attribute list.
        //        Attributes = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(productList, configEntity?.FirstOrDefault()?.ConfigurableAttributeCodes),
        //        productAttributes.SelectedCode,
        //        productAttributes.SelectedValue,
        //        productAttributes.SelectedAttributes,
        //        productList, configEntity?.FirstOrDefault()?.ConfigurableAttributeCodes,
        //        productAttributes.PortalId),
        //    };
        //    /*NIVI CODE*/

        //    // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE for loop started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //    foreach (var item in productAttributes.SelectedAttributes)
        //    {
        //        List<ProductEntity> _newProductList = new List<ProductEntity>();
        //        productList.ForEach(x =>
        //        {
        //            AttributeEntity _Attributes = x.Attributes.FirstOrDefault(y => y.AttributeCode == item.Key && y.SelectValues.FirstOrDefault()?.Value == item.Value);

        //            if (HelperUtility.IsNotNull(_Attributes))
        //                _newProductList.Add(x);
        //        });
        //        productList = _newProductList;
        //    }
        //    //  ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE loop end ." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //    ProductEntity productEntity = productList.FirstOrDefault();

        //    //If Combination does not exist.
        //    if (HelperUtility.IsNull(productEntity))
        //    {
        //        //Get filter
        //        // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE INEtrnal started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //        filters = GetConfigurableProductFilter(productAttributes.LocaleId, configEntity);
        //        productAttributes.SelectedAttributes = new Dictionary<string, string>();
        //        mongoQuery = GetMongoQueryForConfigurableProduct(productAttributes, filters);

        //        //Get Default product.
        //        productEntity = _ProductMongoRepository.GetEntity(Query.And(mongoQuery));
        //        product = productEntity?.ToModel<PublishProductModel>();
        //        //Set IsDefaultConfigurableProduct flag.
        //        if (HelperUtility.IsNotNull(product))
        //            product.IsDefaultConfigurableProduct = true;
        //        // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE INRERNal end started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //    }
        //    else
        //        product = productEntity?.ToModel<PublishProductModel>();

        //    if (HelperUtility.IsNotNull(product))
        //    {
        //        //  ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE PRODUCT Inetrnal not started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //        product.AssociatedGroupProducts = productList.ToModel<WebStoreGroupProductModel>().ToList();

        //        product.ConfigurableProductId = productAttributes.ParentProductId;
        //        product.IsConfigurableProduct = true;

        //        product.ProductType = ZnodeConstant.ConfigurableProduct;
        //        product.ConfigurableProductSKU = product.SKU;
        //        product.SKU = productAttributes.ParentProductSKU;

        //        /*NIVI CODE*/
        //        product.Attributes.Remove(product.Attributes.FirstOrDefault(x => x.AttributeCode == "Color"));
        //        product.Attributes.Remove(product.Attributes.FirstOrDefault(x => x.AttributeCode == "Size"));
        //        product.Attributes.Remove(product.Attributes.FirstOrDefault(x => x.AttributeCode == "ShoeWidth"));
        //        //configurableAttributeListModel.Attributes.ForEach(x =>
        //        //{
        //        //    if(x.AttributeCode!="Color")
        //        //    {
        //        //        x.ConfigurableAttribute.ForEach(y =>
        //        //        {
        //        //            y.DisplayOrder = y.SelectValues.FirstOrDefault().DisplayOrder;
        //        //        });
        //        //    }
        //        //});

        //        product.Attributes.AddRange(configurableAttributeListModel.Attributes);
        //        /*NIVI CODE*/

        //        publishProductHelper.GetDataFromExpands(productAttributes.PortalId, GetExpands(expands), product, productAttributes.LocaleId, string.Empty, GetLoginUserId(), null, null, GetProfileId());
        //        // ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE GetImage started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //        GetProductImagePath(productAttributes.PortalId, product);
        //        //set stored based In Stock, Out Of Stock, Back Order Message.
        //        SetPortalBasedDetails(productAttributes.PortalId, product);
        //        //  ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE SetPortalBased end." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //        /*Nivi Code*/
        //        //  ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE Nivi started." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //        string _swatchImagePath = "";
        //        int index = 0;
        //        if (product.ImageThumbNailPath == null)
        //        {
        //            ImageHelper image = new ImageHelper(productAttributes.PortalId);
        //            string productImage = image.GetImageHttpPathThumbnail("image"); ;
        //            index = productImage.LastIndexOf('/');
        //            if (index != -1)
        //            {
        //                _swatchImagePath = productImage.Substring(0, index) + "/";
        //            }
        //        }
        //        else
        //        {
        //            index = product.ImageThumbNailPath.LastIndexOf('/');
        //            if (index != -1)
        //            {
        //                _swatchImagePath = product.ImageThumbNailPath.Substring(0, index) + "/";
        //            }
        //        }
        //        List<AssociatedProductsModel> _products = (from p in associatedProducts

        //                                                   select new AssociatedProductsModel
        //                                                   {
        //                                                       PublishProductId = p.ZnodeProductId,
        //                                                       //PimProductId = pimProducts.FirstOrDefault(w => w.PublishProductId == p.ZnodeProductId)?.PimProductId,
        //                                                       SKU = p.SKU,
        //                                                       OMSColorCode = p.Attributes.Where(x => x.AttributeCode == ZnodeConstant.GalleryImages)?.FirstOrDefault()?.AttributeValues,
        //                                                       OMSColorValue = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code,
        //                                                       OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
        //                                                       DisplayOrder = p.DisplayOrder,
        //                                                       OMSColorSwatchText = p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
        //                                                       Custom1 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code,
        //                                                       Custom2 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code
        //                                                   }).ToList();
        //        product.AssociatedProducts = new List<AssociatedProductsModel>();
        //        product.AssociatedProducts.AddRange(_products);
        //        string[] codes = productAttributes.Codes.Split(',');
        //        int colorIndex = Array.IndexOf(codes, "Color");
        //        string selectedcolor = productAttributes.Values.Split(',')?[colorIndex];
        //        string prdImage = product.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == selectedcolor)?.OMSColorSwatchText;

        //        if (prdImage != product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues)
        //            GetProductImagesforUnavailableProduct(product, productAttributes.PortalId, selectedcolor);
        //        //   ZnodeLogging.LogMessage("RSPublishProductService-GETCONFIGURABLE Nivi ENd." + DateTime.Now.ToString(), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //        /*Nivi Code*/
        //    }



        //    //ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE-GETCONFIGURABLE PRODUCT FINISHED." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
        //    return product;

        //}
        //public override ConfigurableAttributeListModel GetProductAttribute(int productId, ParameterProductModel model)
        //{
        //    ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
        //    ZnodeLogging.LogMessage("Input Parameter productId:", string.Empty, TraceLevel.Info, new object[] { productId });
        //    if (productId > 0)
        //    {
        //        //Get Configurable product entity.
        //        List<ConfigurableProductEntity> configEntiy = publishProductHelper.GetConfigurableProductEntity(productId, GetCatalogVersionId(model.PublishCatalogId, model.LocaleId));
        //        ZnodeLogging.LogMessage("configEntiy list count:", string.Empty, TraceLevel.Verbose, configEntiy?.Count());
        //        ZnodeLogging.LogMessage($"MapWebStoreConfigurableAttributeData(GetProductAttribute) catalogId {model.PublishCatalogId} productId :{productId} LocaleId {model.LocaleId}", "MapWebStoreConfigurableAttributeData", TraceLevel.Error);

        //        //Get associated configurable product list.
        //        List<ProductEntity> products = publishProductHelper.GetAssociatedProducts(productId, model.LocaleId, GetCatalogVersionId(model.PublishCatalogId, model.LocaleId), configEntiy);
        //        ZnodeLogging.LogMessage("products list count:", string.Empty, TraceLevel.Verbose, products?.Count());
        //        ConfigurableAttributeListModel configurableAttributeListModel = new ConfigurableAttributeListModel()
        //        {
        //            //Get associated configurable product Attribute list.
        //            Attributes = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(products, configEntiy?.FirstOrDefault()?.ConfigurableAttributeCodes),
        //            model.SelectedCode,
        //            model.SelectedValue,
        //            model.SelectedAttributes,
        //            products, configEntiy?.FirstOrDefault()?.ConfigurableAttributeCodes,
        //            model.PortalId),
        //        };
        //        return configurableAttributeListModel;
        //    }
        //    ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
        //    return new ConfigurableAttributeListModel();
        //}
        #endregion

    }
}
