﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service.Service
{
   public class RSSearchService : SearchService
    {
       
        public RSSearchService():base()
        {
        }      
      

        public override void BindProductDetails(KeywordSearchModel searchResult, int portalId, IList<PublishCategoryProductDetailModel> productDetails)
        {
            ImageHelper imageHelper = new ImageHelper(portalId);

            searchResult?.Products?.ForEach(product =>
            {
                PublishCategoryProductDetailModel productSKU = productDetails?
                            .Where(productdata => productdata.SKU == product.SKU)
                            ?.FirstOrDefault();

                if (productSKU!=null)
                {
                    //product.SalesPrice = productSKU.SalesPrice;
                    //product.RetailPrice = productSKU.RetailPrice;
                    product.CurrencyCode = productSKU.CurrencyCode;
                    product.CurrencySuffix = productSKU.CurrencySuffix;
                    product.Quantity = productSKU.Quantity;
                    product.ReOrderLevel = productSKU.ReOrderLevel;
                    product.Rating = productSKU.Rating;
                    product.TotalReviews = productSKU.TotalReviews;
                    product.ImageSmallPath = imageHelper.GetImageHttpPathSmall(product.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues);
                }
            });
        }

        

    }
}
